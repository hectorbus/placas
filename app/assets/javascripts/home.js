$(document).ready(function(){
  $("#file").change(function(e) {
    var url = 'https://api.openalpr.com/v2/recognize?secret_key=sk_e8e179262ff8652032b83fe6&recognize_vehicle=1&country=us&return_image=0&topn=10'
    var fd = new FormData("image", $('#file')[0].files[0])
    fd.append("image", $('#file')[0].files[0])

    $('.plates, .type, .color, .brand, .model').text('');

    if (this.files && this.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('.preview').attr('src', e.target.result);
      }
      reader.readAsDataURL(this.files[0]);
    }

    $('.hide').removeClass('hide');
    $('.loading').show()

    $.ajax({
      type: 'POST',
      url: url,
      data: fd,
      processData: false,
      contentType: false,
      complete: function(){
        $('.loading').hide()
      }
    }).success(function( data ) {
      if(data.epoch_time != 0){
        $('.plates').text(data.results[0].plate).attr('data-content', data.results[0].confidence + '%')
        $('.type').text(data.results[0].vehicle.body_type[0].name).attr('data-content', data.results[0].vehicle.body_type[0].confidence + '%')
        $('.color').text(data.results[0].vehicle.color[0].name).attr('data-content', data.results[0].vehicle.color[0].confidence + '%')
        $('.brand').text(data.results[0].vehicle.make[0].name).attr('data-content', data.results[0].vehicle.make[0].confidence + '%')
        $('.model').text(data.results[0].vehicle.make_model[0].name).attr('data-content', data.results[0].vehicle.make_model[0].confidence + '%')

        $('[data-toggle="popover"]').popover()
      }else{
        alert('ERROR! No se encontro ningun resultado')
      }
    }).error(function() {
      alert('ERROR! No se pudo subir la imagen')
      e.preventDefault();
    })
  })
})
